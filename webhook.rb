# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Load required libraries
# We use sinatra here
require 'sinatra/base'
require "sinatra"
# Allow to read app settings from yaml file
require "sinatra/config_file"
# We define our logging stuff
require 'logger'
# We use a queue runner
require 'sidekiq'
require 'sidekiq/grouping'

# Beware, a global variable.
# Store our main directory so we can use it later to easily find things.
$maindir=File.expand_path(File.dirname(__FILE__))

# Load our config file
config_file ENV.fetch('SALSA_WEBHOOK_CONFIG', File.join($maindir, 'config.yml'))

Sidekiq.configure_server do |config|
  config.redis = { url: settings.redis }
end

Sidekiq.configure_client do |config|
  config.redis = { url: settings.redis }
end

module SalsaWebhook
  # And here goes our actual webapp (hook) logic
  class App < Sinatra::Application

    # Set some defaults, run once at beginning
    configure do
      # queue request for background thread
      set :threaded, true

      # always use thin as web server
      set :server, :thin

      # Do not show exceptions
      set :show_exceptions, false
    end

    # Some defaults.
    before do
      # Return plain text. Gitlab talks with us, gitlab doesn't care
      # what we spit out. Make it plaintext...
      content_type :txt
    end

    # The / endpoint doesn't do anything special, just tells users they
    # are wrong here. Everything that actually does something useful is in
    # a plugin in the plugins dir.
    post '/' do
      # A human trying to talk to us - or someone misconfiguring their hook
      <<-EOERROR
You reached a webhook. That means either you try this as a user in a browser,
which does not do any good, or you misconfigured your webhook settings.
EOERROR
    end
    # And for users with a webbrowser, we support get.
    get '/' do
      # A human trying to talk to us - or someone misconfiguring their hook
      <<-EOERROR
You reached a webhook. That means either you try this as a user in a browser,
which does not do any good, or you misconfigured your webhook settings.
EOERROR
    end
  end
end

# Now load each plugin, which should declare their namespace
Dir.glob('./helpers/*.rb').sort.each { |file| require file }
Dir.glob('./plugins/*.rb').sort.each { |file| require file }
