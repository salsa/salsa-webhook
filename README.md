# GitLab webhooks for Salsa

This project contains the GitLab [webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html)
deployed at https://webhook.salsa.debian.org.

The webhooks available are:

* https://webhook.salsa.debian.org/close/SOURCENAME <br/>
  This hook detects bugs closed in commit messages and closes them in the Debian Bug Tracking System.  
  SOURCENAME is the name of the source package. This webhook accepts the following named parameters:

  - `ignored-namespaces` is a comma-separated list of branch namespaces within the webhook ignores commands.
  You can use branches with the specified prefix to do your preliminary work. After the prefix a branch name
  should contain a separator: `/`, `-`, `_` (or end of line). The default value of the parameter is `wip,pu,people`.
  The acronym "WIP" means "Work in progress", this is a common convention for temporary branches on GitLab. `pu`
  stands for "proposed updates", see [**gitworkflows**(7)](https://manpages.debian.org/gitworkflows.7.html) for
  details. The `people` prefix is intended for personal branches within a main repository.  
  For example, the webhook at https://webhook.salsa.debian.org/close/xdg-utils?ignored-namespaces=pre will ignore
  updates of the `pre/test` or `pre_test` branches in a repository of the xdg-utils package.

* https://webhook.salsa.debian.org/tagpending/SOURCENAME <br/>
  This hook detects bugs closed in commit messages and tag them as pending in the Debian Bug Tracking System.  
  SOURCENAME is the name of the source package. The webhook accepts the following named parameters:

  - `ignored-namespaces` does the same thing for this webhook as explained above.

## Development

To run the webhooks locally:

    apt-get install ruby ruby-mail ruby-sinatra ruby-sinatra-contrib ruby-rack ruby-soap4r thin
    
    rackup
