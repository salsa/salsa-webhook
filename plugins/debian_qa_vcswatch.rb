# Copyright (C) 2018 Bastian Blank <waldi@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'net/http'
require 'net/https'
require 'uri'

module SalsaWebhook
  module DebianQaVcswatch
    TRIGGER_URL = URI('https://qa.debian.org/cgi-bin/vcstrigger')
    VISIBILITY_PUBLIC = 20

    class Worker
      include Sidekiq::Worker

      sidekiq_options(
        batch_flush_interval: 3600,
        batch_unique: true,
      )

      def perform(ev)
        # Extract options hash from each argument list
        data = {'projects': ev.map{|x| x[0]}}
        res = Net::HTTP.start(TRIGGER_URL.hostname, TRIGGER_URL.port, use_ssl: true, ssl_version: :TLSv1_2) do |http|
          req = Net::HTTP::Post.new(TRIGGER_URL, 'Content-Type' => 'application/json')
          req.body = data.to_json
          http.request(req)
        end
        res.value
      end
    end

    def self.registered(app)
      app.event do
        repository_update do
          path = @data.dig('project', 'path_with_namespace')
          visibility_level = @data.dig('project', 'visibility_level')
          git_http_url = @data.dig('project', 'git_http_url')

          if visibility_level != VISIBILITY_PUBLIC
            logger.info "DebianQaVcswatch: Ignore repository_update event for project \"#{path}\" with visibility #{visibility_level}"
          else
            logger.info "DebianQaVcswatch: Process repository_update event for project \"#{path}\""
            Worker.perform_async(git_http_url: git_http_url)
          end
        end
      end
    end
  end

  Systemhook.register DebianQaVcswatch
end
