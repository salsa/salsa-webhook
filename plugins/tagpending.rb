# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'json'
require 'mail'
require 'soap/rpc/driver'
require 'sinatra/base'
require 'sinatra/namespace'

# FIXME: (maybe) Difference in webhook tagpending compared to old alioth
# tagpending: Old alioth version parses debian/changelog of the repo
# to find latest versions bugcloses and is sending mails for them.

# The webhook version here requires the bug closes to be in the commit
# message instead. We do not want direct access to the gits, so
# parsing a file inside there is kinda hard.

module SalsaWebhook
  module Tagpending
    class Worker
      include Sidekiq::Worker

      # Create a SOAP something to talk to the BTS
      @@drv = SOAP::RPC::Driver.new 'https://bugs.debian.org/cgi-bin/soap.cgi', 'Debbugs/SOAP/'
      # And we want to ask for status of a bug
      @@drv.add_method('get_status', 'bugnumber')

      def perform(package, from, commit, bug)
        # Check status of this bug
        bdata = @@drv.get_status(bug)

        # If the bug exists (should hope so) AND it is not done (done
        # field contains an email address == bug is closed)
        if !bdata[bug].nil? && (bdata[bug]['done'].length < 2)
          logger.info("Bug #{bug} open, tagging")
          # Send mail to submitter, template contains Control: magic
          # for the tagging to happen.
          mail = SalsaMail.new do
            from     from
            to       "#{bug}-submitter@bugs.debian.org"
            subject  "Bug##{bug} marked as pending in #{package}"
            body ERB.new(File.read(File.join($maindir, 'templates/tagpending.erb'))).result(binding)
          end
          mail.deliver!
        end # if not bdata[bug]
      end
    end

    def self.registered(app)
      # First, register the Namespace extension
      app.register Sinatra::Namespace

      app.namespace '/tagpending' do
        helpers BugsHelper
        helpers GitlabHookHelper

        before do
          @data = parse_gitlab_push_event
        end

        # We should tag bugs with pending
        post '/:package' do
          # Off into background you go, the following can take long,
          # and gitlab doesn't want to wait.
          # And put package name into easy variable
          package = params['package']

          scan_bugs_push_event(@data) do |commit, from, bug|
            logger.debug("Tagpending for bug ##{bug}")
            Worker.perform_async package, from, commit, bug
          end

          204
        end # post ...
      end
    end
  end

  Sinatra.register Tagpending
end
