# Copyright (C) 2018 Bastian Blank <waldi@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'active_support/security_utils'
require 'json'
require 'sinatra/base'
require 'sinatra/namespace'

# Dispatches System Hook triggers
#
# module SalsaWebhook
#   module SystemhookExample
#     def self.registered(app)
#       app.event do
#         repository_update do
#           logger.info(@data)
#         end
#       end
#     end
#   end
#
#   Systemhook.register SystemhookExample
# end

module SalsaWebhook
  module Systemhook
    def self.registered(app)
      app.register Sinatra::Namespace

      app.namespace '/systemhook' do
        before do
          if request.env['HTTP_X_GITLAB_EVENT'] != 'System Hook'
            halt 403, {'Content-Type' => 'text/plain'}, "Not a Gitlab System Hook event\n"
          end

          secret_token = settings.systemhook_secret_token
          if not secret_token
            halt 503, {'Content-Type' => 'text/plain'}, "System Hook endpoint disabled\n"
          end

          request_token = request.env.fetch('HTTP_X_GITLAB_TOKEN', '')

          if not ActiveSupport::SecurityUtils::secure_compare(request_token, secret_token)
            halt 403, {'Content-Type' => 'text/plain'}, "Invalid Gitlab Token\n"
          end

          begin
            @data = JSON.parse(request.body.read)
          rescue JSON::ParserError => e
            halt 418, {'Content-Type' => 'text/plain'}, "Failed to parse JSON payload: #{e.message}\n"
          end
        end

        post '/' do
          event_name = @data['event_name']
          @@routes.fetch(event_name.to_sym, []).each do |event, block|
            begin
              block[self]
            rescue ::Exception => boom
              dump_errors!(boom)
            end
          end
          nil
        end
      end
    end

    class << self
      @@handlers = []
      @@routes = {}

      def register(*handlers)
        @@handlers += handlers
        handlers.each do |handler|
          handler.registered(self) if handler.respond_to?(:registered)
        end
      end

      # Run code in context of Sinatra application
      def generate_method(method_name, &block)
        method_name = method_name.to_sym
        define_method(method_name, &block)
        method = instance_method method_name
        remove_method method_name
        method
      end

      def route(type, event, &block)
        unbound_method = generate_method(type, &block)
        wrapper = block.arity != 0 ?
          proc { |a,p| unbound_method.bind(a).call(*p) } :
          proc { |a,p| unbound_method.bind(a).call }
        (@@routes[type] ||= []) << [ event, wrapper ]
        nil
      end

      def event(&block)
        Event.new(self, &block)
      end
    end

    module Event
      def self.new(base, &block)
        Module.new do
          @base = base
          extend EventMethods
          class_eval(&block)
        end
      end

      module EventMethods
        EVENTS = [
          :group_create,
          :group_destroy,
          :group_rename,
          :project_create,
          :project_destroy,
          :project_rename,
          :project_transfer,
          :project_update,
          :user_add_to_group,
          :user_create,
          :user_destroy,
          :user_failed_login,
          :user_remove_from_group,
          :user_rename,
          :user_update_for_group,

          :push,
          :repository_update,
          :tag_push,
        ]

        EVENTS.each do |event|
          define_method(event) do |&block|
            @base.route(event, self, &block)
          end
        end
      end
    end
  end

  Sinatra.register Systemhook
end
