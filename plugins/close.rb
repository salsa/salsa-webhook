# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'json'
require 'mail'
require 'sidekiq'
require 'sinatra/base'
require 'sinatra/namespace'

module SalsaWebhook
  module Close
    def self.registered(app)
      # First, register the Namespace extension
      app.register Sinatra::Namespace

      app.namespace '/close' do
        helpers BugsHelper
        helpers GitlabHookHelper

        before do
          @data = parse_gitlab_push_event
        end

        # We should close bugs in Debian BTS
        post '/:package' do
          # And put package name into easy variable
          package = params['package']

          scan_bugs_push_event(@data) do |commit, from, bug|
            logger.debug "Closing bug: #{bug}"
            # Send mail to -done@b.d.o
            mail = SalsaMail.new do
              from     from
              to       "#{bug}-done@bugs.debian.org"
              subject  "Bug##{bug} fixed in #{package}"
              body ERB.new(File.read(File.join($maindir, 'templates/close.erb'))).result(binding)
            end
            mail.deliver!
          end

          204
        end # post ...
      end
    end
  end

  Sinatra.register Close
end
