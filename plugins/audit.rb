# Copyright (C) 2020 Bastian Blank <waldi@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

module SalsaWebhook
  module Audit
    def self.registered(app)
      audit = Proc.new do
        puts "event #{@data}"
        audit_log_file = settings.audit_log_file
        return unless audit_log_file

        File.open(File.expand_path(audit_log_file), 'a') do |f|
          f.write(JSON.dump(@data) + "\n")
        end
      end

      app.event do
        group_create(&audit)
        group_destroy(&audit)
        group_rename(&audit)
        user_add_to_group(&audit)
        user_create(&audit)
        user_destroy(&audit)
        user_failed_login(&audit)
        user_remove_from_group(&audit)
        user_rename(&audit)
        user_update_for_group(&audit)
      end
    end
  end

  Systemhook.register Audit
end
