# Copyright (C) 2018 Bastian Blank <waldi@debian.org>
#               2018 Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'mail'

module SalsaWebhook
  class SalsaMail < Mail::Message
    DEFAULT_SENDER = 'noreply@salsa.debian.org'

    def initialize
      super()

      sender(SalsaMail.default_sender)
      header['Auto-Submitted'] = 'auto-generated'

      delivery_method :sendmail
    end

    def self.default_sender
      DEFAULT_SENDER
    end
  end
end
