# Copyright (C) 2018 Bastian Blank <waldi@debian.org>
#               2018 Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module SalsaWebhook
  module BugsHelper
    SCAN_BUGS = /Closes:\s+(?:Bug)?#(?:(\d{4,8})\b)(?:,?\s*(?:Bug)?#(?:(\d{4,8})\b))*/i

    def scan_bugs_push_event(data)
      # Ignore non push updates.
      return if data['object_kind'] != 'push'

      # Skip pushes into ignored branches.
      ignored_namespaces = params.fetch('ignored-namespaces', 'wip,pu,people').downcase.split(',')
      prefixes = ignored_namespaces.product('/_-'.chars).map { |a, b| a + b }
      branch_name = data['ref'].downcase.sub(/^refs\/heads\//, '')
      return if ignored_namespaces.include?(branch_name) or branch_name.start_with?(*prefixes)

      from = Mail::Address.new
      from.display_name = data['user_name']
      from.address = data['user_email']
      if from.address.nil? or from.address.empty?
        from.address = SalsaWebhook::SalsaMail.default_sender
      end

      data['commits'].each do |commit|
        commit['message'].each_line do |line|
          line.scan(SCAN_BUGS).flatten.compact.each do |bug|
            yield commit, from.to_s, bug.to_i
          end
        end
      end
    end
  end
end
