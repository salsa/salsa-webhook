# Copyright (C) 2018 Bastian Blank <waldi@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module SalsaWebhook
  module GitlabHookHelper
    def parse_gitlab_push_event
      if !['Push Hook', 'Tag Push Hook'].include? request.env['HTTP_X_GITLAB_EVENT']
        halt 403, {'Content-Type' => 'text/plain'}, "Not a Gitlab Push Hook event\n"
      end

      begin
        data = JSON.parse(request.body.read)
      rescue JSON::ParserError => e
        halt 418, {'Content-Type' => 'text/plain'}, "Failed to parse JSON payload: #{e.message}\n"
      end

      data
    end
  end
end
